<?php

namespace App;
use Storage;


use Illuminate\Database\Eloquent\Model;

class tempatSampah extends Model
{
    protected $table = 'tempat_sampah';
    protected $guarded = ['id'];

    protected $appends = ['tinggi_sampah', 'kategori'];

    function getTinggiSampahAttribute() {
	  return 100-$this->status;
	}

	function getKategoriAttribute(){
		if($this->status>75){
				return 'Low';
		} else if($this->status>45 && $this->status<75){
			return 'Medium';
		} else {
			return 'High';
		}
	}

}
